const workElement = document.getElementById("work");
const laptopsElement = document.getElementById("laptops");
const earnElement = document.getElementById("earn");
const balanceElement = document.getElementById("balance");
const loanButtonElement = document.getElementById("loanButton");
const payLoanButtonElement = document.getElementById("payLoanButton");
const takenLoanElement = document.getElementById("takenLoan");
const priceElement = document.getElementById("price");
const nameElement = document.getElementById("name");
const descriptionElement = document.getElementById("description");
const imageElement = document.getElementById("image");
const buyElement = document.getElementById("buy");
const specsElement = document.getElementById("specs");

let laptops = [];

let earn = 0;
let balance = 0;
let loan = 0;
let price = 0;

earnElement.innerHTML = earn;
balanceElement.innerHTML = balance;
takenLoanElement.innerHTML = loan;

/*
Fetching data API from link.
*/
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToMenu(laptops))
/*
Giving elements default value to start on (first index from array)
*/ 
const addLaptopsToMenu = (laptops) => {
    laptops.forEach(x => addLaptopToMenu(x));
    priceElement.innerHTML = laptops[0].price + ` KR`;
    nameElement.innerHTML = laptops[0].title;
    descriptionElement.innerHTML = laptops[0].description;
    specsElement.innerHTML = laptops[0].specs;
    image.setAttribute(
        'src',
        `https://noroff-komputer-store-api.herokuapp.com/${laptops[0].image}`
    );
}
/*
Creating option elements into my html selection.
*/
const addLaptopToMenu = (laptop) => {
    const laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopsElement.appendChild(laptopElement);
}
/*
This function helps to change laptop information who gets displayed, once you chose a different laptop from the select element. 
*/
const handleComputerMenuChange = e => {
    const selectedComputer = laptops[e.target.selectedIndex];
    priceElement.innerText = selectedComputer.price + ` KR`;
    nameElement.innerHTML = selectedComputer.title;
    descriptionElement.innerHTML = selectedComputer.description;

    const image = document.getElementById("image");
    image.setAttribute(
        'src',
        `https://noroff-komputer-store-api.herokuapp.com/${selectedComputer.image}`
    );
}
/*
This function helps to view the specification of the laptop. 
*/
const specsChange = e => {
    const selectedComputer = laptops[e.target.selectedIndex];
    specsElement.innerText = selectedComputer.specs;
}
/*
This function handles the loan, first it checks if you have a loan since before if you do you will get 
an alert that you can't have ask for more than one loan at the time. If you don't have an existing loan, 
it will ask how much you would like to loan, you can only type in numbers. Gives you an alert if you ask
for a loan who is higher than double your balance. 
*/
const handleLoan = () => {
    if (loan > 0) {
        alert(`You can't get more than one bank loan before repaying the last loan!`) 
    }
    else {
    const askedLoan = parseInt(prompt("Please enter the amount of money you wish to loan: "));
     if (isNaN(askedLoan)) {
     alert("Has to be number")   
     } else if(askedLoan > balance*2){
        alert(`You can't get a loan more than double of your bank balance!`)
     }else{
        balance += askedLoan
        loan += askedLoan
        takenLoanElement.innerText = loan;
        alert(`Your balance now is: ${balance}`)
        payLoanButtonElement.style.display = "inline"; 
     }
    }    
    console.log(balance);
    balanceElement.innerHTML = `${balance}`
}

function workButton(){
    earnElement.innerHTML = earn += 100;
}
/*
This function transfers the earned money into balance.
First it checks if you have a loan, if you do it will first check if the loan is less than the 10% if it is 
it will do the if statement which checks and adds the difference. Otherwise if loan is not less than 10% it will just 
just do the calculation.
*/
const bankButton = () => {
    if(loan > 0){
        if (loan <= 0.1 * earn ) {
            
            let diff = 0.1* earn - parseInt(loan)  

            balance += earn * 0.9 + diff

            loan = 0
            earn = 0

            balanceElement.innerHTML = balance
            earnElement.innerHTML = earn
            takenLoanElement.innerHTML = loan
            payLoanButtonElement.style.display = "none";
        } else {
            loan -= earn * 0.1;
            takenLoanElement.innerHTML = loan;
            earn = earn * 0.9;
            balance = balance + earn;
            balanceElement.innerHTML = balance;
            earn = 0;
            earnElement.innerHTML = earn;
        }
       
    }else{
        balanceElement.innerHTML = balance += earn;
        earn = 0;
        earnElement.innerHTML = earn;

    }
}
/*
This function takes the balance and takes the price of the computer and checks if you can buy it or not, 
if you can't buy it will show an alert that you got not enough money. If you can it will reduce your balance 
depending on the laptop price and it will show an alert that your purchase have been done.
*/
const buyButton = () => {
    const computerPrice = laptops[laptopsElement.selectedIndex];
    price = computerPrice.price;

    if(balance >= price){
        balance = balance - price;
        balanceElement.innerHTML = balance;
        alert(`Your purchase have been done!`)
    }else{
        alert(`You don't have enough money to buy this!`)
    }
}
/*
This function helps to pay the loan by how much you have earned, if you pay full loan the button will 
disappear if your earned is more than the loan the rest will go to your balance. 
*/
const payLoan = () => {
    let rest;
    if (earn >= loan) {
        rest = earn - loan;
        balance += rest;
        earn = 0;
        loan = 0;
        rest = 0;
        earnElement.innerHTML = earn;
        balanceElement.innerHTML = balance;
        takenLoanElement.innerHTML = loan;
        payLoanButtonElement.style.display = "none";
    } else if(earn < loan){
        rest = loan - earn;
        loan = rest;
        earn = 0;
        earnElement.innerHTML = earn;
        takenLoanElement.innerHTML = loan;
        
    }
}
/*
Event listeners who handle on click and change for functions.
*/
loanButtonElement.addEventListener("click", handleLoan);
laptopsElement.addEventListener("change", handleComputerMenuChange);
laptopsElement.addEventListener("change", specsChange);